package it.uniparthenope.reconosp;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by dlubrano on 26/07/15.
 */
public class HudItemView extends RelativeLayout {

    public HudItemView(Context context) {
        super(context);
        init(null);
    }

    public HudItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public HudItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private TextView mTxtValue;
    private TextView mTxtLabel;
    private TextView mTxtMark;

    private void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.hud_item, this);

        mTxtValue = (TextView) findViewById(R.id.value);
        mTxtLabel = (TextView) findViewById(R.id.label);
        mTxtMark = (TextView) findViewById(R.id.mark);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HudItemView);

            setLabel(a.getString(R.styleable.HudItemView_label));
            setValue(a.getString(R.styleable.HudItemView_value));
            setMark(a.getString(R.styleable.HudItemView_mark));



            a.recycle();
        }

    }


    public void setValue(String value){
        mTxtValue.setText(value);
    }

    public void setLabel(String label){
        mTxtLabel.setText(label);
    }

    public void setMark(String mark){
        mTxtMark.setText(mark);
    }

}
