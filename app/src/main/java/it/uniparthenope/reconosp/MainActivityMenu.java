package it.uniparthenope.reconosp;

import android.content.Context;
import android.content.Intent;

import com.reconinstruments.ui.carousel.CarouselActivity;
import com.reconinstruments.ui.carousel.CarouselItem;
import com.reconinstruments.ui.carousel.StandardCarouselItem;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dlubrano on 23/07/15.
 */
public class MainActivityMenu extends CarouselActivity{

    @Override
    public int getLayoutId() {
        return R.layout.carousel_host;
    }

    static class ImageCarouselItem extends StandardCarouselItem {
        Class activityClass;
        public ImageCarouselItem(String title, Integer icon,Class activityClass) {
            super(title, icon);
            this.activityClass = activityClass;
        }
        @Override
        public int getLayoutId() {
            return R.layout.carousel_item_title_icon_column;
        }
        @Override
        public void onClick(Context context) {
            context.startActivity(new Intent(context, activityClass));
        }
    }

    CarouselItem[] items = {
            new ImageCarouselItem("HUD OSP",R.drawable.carousel_icon_newactivity,HUDOSP.class),
            //new ImageCarouselItem("TestWebApi",R.drawable.carousel_icon_myapps,TestWebApi.class),
            new ImageCarouselItem("Settings",R.drawable.carousel_icon_settings,this.getClass())
    };

    @Override
    public List<? extends CarouselItem> createContents() {
        return Arrays.asList(items);
    }
}
