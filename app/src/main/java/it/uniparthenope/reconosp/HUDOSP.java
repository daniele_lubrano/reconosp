package it.uniparthenope.reconosp;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.reconinstruments.webapi.IReconHttpCallback;
import com.reconinstruments.webapi.ReconHttpRequest;
import com.reconinstruments.webapi.ReconHttpResponse;
import com.reconinstruments.webapi.ReconOSHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HUDOSP extends FragmentActivity {

    private final int REFRESH_RATE_MS = 8000;

    private static final String TAG = HUDOSP.class.getSimpleName();

    //ArrayList<HudItemView> HUDItems = new ArrayList<HudItemView>();
    HashMap<Integer,HudItemView> HUDItemsMap = new HashMap<Integer,HudItemView>();
    private android.os.Handler mHandler = new android.os.Handler();


    ArrayList<String> incomingData = new ArrayList<String>();
    //final ArrayList<String> mData = new ArrayList<String>();
    final HashMap<String,String> mDataMap = new HashMap<String,String>();

    private ReconOSHttpClient client;


    private Runnable mRefreshData = new Runnable() {
        @Override
        public void run() {
            makeRequest();
            updateUI();
            mHandler.postDelayed(this,REFRESH_RATE_MS);
        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_hudos);

        client = new ReconOSHttpClient(this, clientCallback);


        HUDItemsMap.put(R.id.Latitude, (HudItemView) findViewById(R.id.Latitude));
        HUDItemsMap.put(R.id.Longitude, (HudItemView) findViewById(R.id.Longitude));
        HUDItemsMap.put(R.id.WindSpeed, (HudItemView) findViewById(R.id.WindSpeed));
        HUDItemsMap.put(R.id.Speed, (HudItemView) findViewById(R.id.Speed));
        HUDItemsMap.put(R.id.WPLatitude, (HudItemView) findViewById(R.id.WPLatitude));
        HUDItemsMap.put(R.id.WPLongitude, (HudItemView) findViewById(R.id.WPLongitude));
/*
        HUDItems.add((HudItemView) findViewById(R.id.Latitude));
        HUDItems.add((HudItemView)findViewById(R.id.Longitude));
        HUDItems.add((HudItemView)findViewById(R.id.WindSpeed));
        HUDItems.add((HudItemView)findViewById(R.id.Speed));
        HUDItems.add((HudItemView)findViewById(R.id.WPLatitude));
        HUDItems.add((HudItemView)findViewById(R.id.WPLongitude));

*/

        mRefreshData.run();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        client.close();
    }


    private void updateUI(){
        HUDItemsMap.get(R.id.Latitude).setMark(mDataMap.get("lat_dir"));
        HUDItemsMap.get(R.id.Latitude).setValue(mDataMap.get("latitude"));

        HUDItemsMap.get(R.id.Longitude).setMark(mDataMap.get("lon_dir"));
        HUDItemsMap.get(R.id.Longitude).setValue(mDataMap.get("longitude"));

        HUDItemsMap.get(R.id.WindSpeed).setMark("m/s");
        HUDItemsMap.get(R.id.WindSpeed).setValue(mDataMap.get("wind_speed_true"));

        HUDItemsMap.get(R.id.Speed).setMark("KN");
        HUDItemsMap.get(R.id.Speed).setValue(mDataMap.get("water_speed"));

        HUDItemsMap.get(R.id.WPLatitude).setMark(mDataMap.get("wp_lat_dir"));
        HUDItemsMap.get(R.id.WPLatitude).setValue(mDataMap.get("waypoint_lat"));

        HUDItemsMap.get(R.id.WPLongitude).setMark(mDataMap.get("wp_lon_dir"));
        HUDItemsMap.get(R.id.WPLongitude).setValue(mDataMap.get("waypoint_lon"));

    }

    public void makeRequest() {
        Log.d(TAG, "Fetching data...");
        try {
            //URL url = new URL("http://web.uniparthenope.it:13401/fw/data/query/all");
            URL url = new URL("http://potatodanpc.no-ip.org/response.json");
            Map<String,List<String>> headers = new HashMap<String,List<String>>();
            client.sendRequest(new ReconHttpRequest("GET",url,15000,headers,null));
            //GET RESPONSE VIA CALLBACK INTERFACE
        } catch (MalformedURLException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void sendRequest(ReconHttpRequest request) {
        if (-1 == client.sendRequest(request)) {
            Toast.makeText(HUDOSP.this, "HUD not connected", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(HUDOSP.this, "Request Sent", Toast.LENGTH_SHORT).show();
            Log.d(TAG,"Request Sent");
        }
    }

    private IReconHttpCallback clientCallback = new IReconHttpCallback() {
        @Override
        public void onReceive(int requestId, ReconHttpResponse response) {
            //Toast.makeText(HUDOSP.this, response.toString(), Toast.LENGTH_SHORT).show();
            Log.d(TAG,"Callback reached");
            String responseBody = "";

            try {
                responseBody = new String(response.getBody(), "UTF-8");

                JSONObject respJson = new JSONObject(responseBody);
                if (!(respJson.get("result").toString() == "null")){

                    JSONObject data = respJson.getJSONArray("result").getJSONObject(0).getJSONObject("data");
                    //JSONObject data = respJson.getJSONObject("result");
                    //String placeName = data.getJSONObject("long_name").get("it").toString();

                    Log.i("Response String", data.toString());

                    mDataMap.put("water_speed", data.getJSONObject("water").getString("water_speed"));

                    mDataMap.put("wind_speed_true",data.getJSONObject("wind").getString("wind_speed_true"));

                    mDataMap.put("lat_dir",data.getJSONObject("position").getString("lat_dir"));
                    mDataMap.put("latitude",data.getJSONObject("position").getString("latitude"));

                    mDataMap.put("lon_dir",data.getJSONObject("position").getString("lon_dir"));
                    mDataMap.put("longitude",data.getJSONObject("position").getString("longitude"));

                    mDataMap.put("wp_lat_dir",data.getJSONObject("wp_position").getString("wp_lat_dir"));
                    mDataMap.put("waypoint_lat",data.getJSONObject("wp_position").getString("waypoint_lat"));

                    mDataMap.put("wp_lon_dir",data.getJSONObject("wp_position").getString("wp_lon_dir"));
                    mDataMap.put("waypoint_lon",data.getJSONObject("wp_position").getString("waypoint_lon"));
/*
                    mData.add(data.getJSONObject("water").getString("water_speed"));

                    mData.add(data.getJSONObject("wind").getString("wind_speed_true"));

                    mData.add(data.getJSONObject("position").getString("lat_dir"));
                    mData.add(data.getJSONObject("position").getString("latitude"));

                    mData.add(data.getJSONObject("position").getString("lon_dir"));
                    mData.add(data.getJSONObject("position").getString("longitude"));

                    mData.add(data.getJSONObject("wp_position").getString("wp_lat_dir"));
                    mData.add(data.getJSONObject("wp_position").getString("waypoint_lat"));

                    mData.add(data.getJSONObject("wp_position").getString("wp_lon_dir"));
                    mData.add(data.getJSONObject("wp_position").getString("waypoint_lon"));
*/

                }
                else
                    Log.e(TAG, "API Malfunctioning, NULL response");

            } catch (JSONException | UnsupportedEncodingException e ) {
                Log.e(TAG, "Error in parsing response: " + e.getMessage());
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int requestId, ERROR_TYPE type, String message) {
            Toast.makeText(HUDOSP.this, "Error making request: " + type.toString() + "(" + message + ")", Toast.LENGTH_SHORT).show();
        }

    };

}
