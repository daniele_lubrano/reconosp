package it.uniparthenope.reconosp;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.reconinstruments.ui.carousel.CarouselItem;
import com.reconinstruments.ui.carousel.StandardCarouselItem;
import com.reconinstruments.ui.dialog.BaseDialog;
import com.reconinstruments.ui.dialog.CarouselDialog;
import com.reconinstruments.ui.dialog.DialogBuilder;
import com.reconinstruments.ui.list.SimpleListActivity;
import com.reconinstruments.ui.list.SimpleListItem;
import com.reconinstruments.ui.list.StandardListItem;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import com.reconinstruments.webapi.IReconHttpCallback;
import com.reconinstruments.webapi.ReconHttpRequest;
import com.reconinstruments.webapi.ReconHttpResponse;
import com.reconinstruments.webapi.ReconOSHttpClient;

/**
 * Created by dlubrano on 24/07/15.
 */
public class TestWebApi extends SimpleListActivity {

    public class ListItem extends StandardListItem {
        String subtitle;
        OnClickCallback callback;
        public ListItem(String text, OnClickCallback callback){
            super(text);
            this.callback = callback;
        }
        public void onClick() {
            callback.onClick(this);
        }
        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
            TextView subtitleView = (TextView)getView().findViewById(R.id.subtitle);
            subtitleView.setVisibility(View.VISIBLE);
            subtitleView.setText(subtitle);
        }
        public String getSubtitle() {
            return subtitle;
        }
    }
    public interface OnClickCallback {
        public void onClick(ListItem item);
    }

    SimpleListItem[] items = {
            new ListItem("Test Connection",new OnClickCallback() {
                public void onClick(ListItem item) {
                    //createBasicDialog();
                    goodRequest();
                }
            }),
            new ListItem("Test WebApi request",new OnClickCallback() {
                public void onClick(ListItem item) {
                    createProgressDialog();
                }
            })
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // The idea is that you instantiate an http client
        // object with a call back and then keep feeding it ReconHttpRequests
        client = new ReconOSHttpClient(this, clientCallback);
    }

    @Override
    public List<SimpleListItem> createContents() {
        return Arrays.asList(items);
    }

    private void createBasicDialog(String diagTitle,String diagSub) {

        new DialogBuilder(this).setTitle(diagTitle).setSubtitle(diagSub).setLayout(R.layout.action_bar_dialog).createDialog().show();
    }

    private void createProgressDialog() {
        new DialogBuilder(this).setTitle("Loading").setSubtitle("(press select to finish)").showProgress().setOnKeyListener(new BaseDialog.OnKeyListener() {
            @Override
            public boolean onKey(BaseDialog dialog, int keyCode, KeyEvent event) {
                if (event.getAction()==KeyEvent.ACTION_UP&&keyCode==KeyEvent.KEYCODE_DPAD_CENTER) {
                    ImageView icon = (ImageView)dialog.getView().findViewById(R.id.icon);
                    icon.setImageResource(R.drawable.icon_checkmark);
                    icon.setVisibility(View.VISIBLE);
                    dialog.getView().findViewById(R.id.progress_bar).setVisibility(View.GONE);
                    dialog.setDismissTimeout(2);
                    return true;
                }
                return false;
            }
        }).createDialog().show();
    }

    private static final String TAG = TestWebApi.class.getSimpleName();
    private ReconOSHttpClient client;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        client.close();
    }

    public void goodRequest() {
        Log.d(TAG, "Fetching data...");
        try {
            URL url = new URL("http://localhost:8080/response.json");
            Map<String,List<String>> headers = new HashMap<String,List<String>>();
            client.sendRequest(new ReconHttpRequest("GET",url,15000,headers,null));
            //GET RESPONSE VIA CALLBACK
        } catch (MalformedURLException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void sendRequest(ReconHttpRequest request) {
        if (-1 == client.sendRequest(request)) {
            Toast.makeText(TestWebApi.this, "HUD not connected", Toast.LENGTH_SHORT).show();
            //textView.setText("");
        } else {
            Toast.makeText(TestWebApi.this, "Request Sent", Toast.LENGTH_SHORT).show();
        }
    }

    private IReconHttpCallback clientCallback = new IReconHttpCallback() {
        @Override
        public void onReceive(int requestId, ReconHttpResponse response) {
            Toast.makeText(TestWebApi.this, response.toString(), Toast.LENGTH_SHORT).show();

            String responseBody = "";

            try {
                responseBody = new String(response.getBody(), "UTF-8");
                JSONObject respJson = new JSONObject(responseBody);
                if (respJson.get("result").toString() == "null"){


                    JSONObject data = respJson.getJSONObject("result");
                    String placeName = data.getJSONObject("long_name").get("it").toString();
                    Log.d("Response Body String", placeName);
                    createBasicDialog("Response", placeName);

                }
            } catch (JSONException | UnsupportedEncodingException e ) {
                Log.e(TAG, "Error in parsing response: " + e.getMessage());
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int requestId, ERROR_TYPE type, String message) {
            //Toast.makeText(TestWebApi.this, "Error: " + type.toString() + "(" + message + ")", Toast.LENGTH_SHORT).show();
            //textView.setText("");
            createBasicDialog("Error: " + type.toString(), message);
        }

    };
}