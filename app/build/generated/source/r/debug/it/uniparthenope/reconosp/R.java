/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package it.uniparthenope.reconosp;

public final class R {
    public static final class anim {
        public static final int carousel_item_push=0x7f050000;
        public static final int fade_in_slide_in_bottom=0x7f050001;
        public static final int fade_out=0x7f050002;
    }
    public static final class attr {
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int actionText=0x7f010000;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int animateSelection=0x7f010004;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonIcon=0x7f010001;
        /** <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>select</code></td><td>0</td><td></td></tr>
<tr><td><code>back</code></td><td>1</td><td></td></tr>
<tr><td><code>up</code></td><td>2</td><td></td></tr>
<tr><td><code>down</code></td><td>3</td><td></td></tr>
<tr><td><code>updown</code></td><td>4</td><td></td></tr>
</table>
         */
        public static final int buttonType=0x7f010002;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int fontResource=0x7f01000a;
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int label=0x7f010008;
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int mark=0x7f010009;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pageMargin=0x7f010005;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pageWidth=0x7f010006;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int showBreadcrumbs=0x7f010003;
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int value=0x7f010007;
    }
    public static final class color {
        public static final int recon_jet_background=0x7f070000;
        public static final int recon_jet_background_translucent=0x7f070001;
        public static final int recon_jet_gray=0x7f070002;
        public static final int recon_jet_highlight=0x7f070003;
        public static final int recon_jet_text_primary=0x7f070004;
        public static final int recon_jet_text_secondary=0x7f070005;
        public static final int recon_jet_white=0x7f070006;
    }
    public static final class drawable {
        public static final int breadcrumb_square_dark=0x7f020000;
        public static final int breadcrumb_square_white=0x7f020001;
        public static final int carousel_icon_myapps=0x7f020002;
        public static final int carousel_icon_newactivity=0x7f020003;
        public static final int carousel_icon_settings=0x7f020004;
        public static final int checkbox_disabled_grey=0x7f020005;
        public static final int checkbox_disabled_orange=0x7f020006;
        public static final int checkbox_enabled_grey=0x7f020007;
        public static final int checkbox_enabled_orange=0x7f020008;
        public static final int icon_action_back=0x7f020009;
        public static final int icon_action_down=0x7f02000a;
        public static final int icon_action_select=0x7f02000b;
        public static final int icon_action_select_remote=0x7f02000c;
        public static final int icon_action_up_down=0x7f02000d;
        public static final int icon_checkmark=0x7f02000e;
        public static final int icon_warning=0x7f02000f;
        public static final int loading_spinner=0x7f020010;
        public static final int recon_loading_spinner=0x7f020011;
        public static final int selectable_checkbox_disabled=0x7f020012;
        public static final int selectable_checkbox_enabled=0x7f020013;
        public static final int selectable_checkmark_hidden=0x7f020014;
        public static final int selectable_text_highlight_color=0x7f020015;
        public static final int selectable_text_highlight_hidden=0x7f020016;
        public static final int selectable_text_highlight_white=0x7f020017;
    }
    public static final class id {
        public static final int Latitude=0x7f0b0011;
        public static final int Longitude=0x7f0b0012;
        public static final int Speed=0x7f0b0014;
        public static final int WPLatitude=0x7f0b0015;
        public static final int WPLongitude=0x7f0b0016;
        public static final int WindSpeed=0x7f0b0013;
        public static final int action_bar=0x7f0b0007;
        public static final int action_settings=0x7f0b001b;
        public static final int back=0x7f0b0000;
        public static final int button_action_icon=0x7f0b000a;
        public static final int button_action_title=0x7f0b000b;
        public static final int carousel=0x7f0b000d;
        public static final int content=0x7f0b0008;
        public static final int description=0x7f0b0009;
        public static final int down=0x7f0b0001;
        public static final int icon=0x7f0b000f;
        public static final int image_view=0x7f0b000e;
        public static final int label=0x7f0b0018;
        public static final int mark=0x7f0b0019;
        public static final int progress_bar=0x7f0b0010;
        public static final int select=0x7f0b0002;
        public static final int subicon=0x7f0b001a;
        public static final int subtitle=0x7f0b0006;
        public static final int title=0x7f0b0005;
        public static final int up=0x7f0b0003;
        public static final int updown=0x7f0b0004;
        public static final int value=0x7f0b0017;
        public static final int view_pager=0x7f0b000c;
    }
    public static final class layout {
        public static final int action_bar_dialog=0x7f040000;
        public static final int action_bar_example=0x7f040001;
        public static final int button_action=0x7f040002;
        public static final int carousel_fragment=0x7f040003;
        public static final int carousel_host=0x7f040004;
        public static final int carousel_host_bold_text=0x7f040005;
        public static final int carousel_host_divider=0x7f040006;
        public static final int carousel_host_title_body=0x7f040007;
        public static final int carousel_item_hidden_checkmark=0x7f040008;
        public static final int carousel_item_icon=0x7f040009;
        public static final int carousel_item_title=0x7f04000a;
        public static final int carousel_item_title_icon_column=0x7f04000b;
        public static final int carousel_item_title_icon_row=0x7f04000c;
        public static final int dialog_standard=0x7f04000d;
        public static final int fragment_hudos=0x7f04000e;
        public static final int hud_item=0x7f04000f;
        public static final int list_item_standard_layout=0x7f040010;
        public static final int list_standard_layout=0x7f040011;
    }
    public static final class menu {
        public static final int menu_hudosp=0x7f0a0000;
    }
    public static final class mipmap {
        public static final int ic_launcher=0x7f030000;
    }
    public static final class raw {
        public static final int opensans_bold=0x7f060000;
        public static final int opensans_light=0x7f060001;
        public static final int opensans_regular=0x7f060002;
        public static final int opensans_semibold=0x7f060003;
    }
    public static final class string {
        public static final int action_settings=0x7f080000;
        public static final int app_name=0x7f080001;
        public static final int hello_world=0x7f080002;
        public static final int title_activity_hudosp=0x7f080003;
    }
    public static final class style {
        /**  Customize your theme here. 
         */
        public static final int AppTheme=0x7f090000;
        public static final int ReconTheme=0x7f090001;
        public static final int ReconTheme_Dialog=0x7f090002;
        public static final int ReconTheme_Fullscreen=0x7f090003;
        public static final int recon_button_action=0x7f090004;
        public static final int recon_button_action_bar=0x7f090005;
        public static final int recon_dialog_animation=0x7f090006;
        public static final int recon_horizontal_divider=0x7f090007;
        public static final int recon_jet_text_primary=0x7f090008;
        public static final int recon_jet_text_secondary=0x7f090009;
        public static final int recon_jet_text_selectable=0x7f09000a;
        public static final int recon_list=0x7f09000b;
    }
    public static final class styleable {
        /** Attributes that can be used with a ButtonActionView.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #ButtonActionView_actionText it.uniparthenope.reconosp:actionText}</code></td><td></td></tr>
           <tr><td><code>{@link #ButtonActionView_buttonIcon it.uniparthenope.reconosp:buttonIcon}</code></td><td></td></tr>
           <tr><td><code>{@link #ButtonActionView_buttonType it.uniparthenope.reconosp:buttonType}</code></td><td></td></tr>
           </table>
           @see #ButtonActionView_actionText
           @see #ButtonActionView_buttonIcon
           @see #ButtonActionView_buttonType
         */
        public static final int[] ButtonActionView = {
            0x7f010000, 0x7f010001, 0x7f010002
        };
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#actionText}
          attribute's value can be found in the {@link #ButtonActionView} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name it.uniparthenope.reconosp:actionText
        */
        public static final int ButtonActionView_actionText = 0;
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#buttonIcon}
          attribute's value can be found in the {@link #ButtonActionView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name it.uniparthenope.reconosp:buttonIcon
        */
        public static final int ButtonActionView_buttonIcon = 1;
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#buttonType}
          attribute's value can be found in the {@link #ButtonActionView} array.


          <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>select</code></td><td>0</td><td></td></tr>
<tr><td><code>back</code></td><td>1</td><td></td></tr>
<tr><td><code>up</code></td><td>2</td><td></td></tr>
<tr><td><code>down</code></td><td>3</td><td></td></tr>
<tr><td><code>updown</code></td><td>4</td><td></td></tr>
</table>
          @attr name it.uniparthenope.reconosp:buttonType
        */
        public static final int ButtonActionView_buttonType = 2;
        /** Attributes that can be used with a CarouselViewPager.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CarouselViewPager_animateSelection it.uniparthenope.reconosp:animateSelection}</code></td><td></td></tr>
           <tr><td><code>{@link #CarouselViewPager_pageMargin it.uniparthenope.reconosp:pageMargin}</code></td><td></td></tr>
           <tr><td><code>{@link #CarouselViewPager_pageWidth it.uniparthenope.reconosp:pageWidth}</code></td><td></td></tr>
           <tr><td><code>{@link #CarouselViewPager_showBreadcrumbs it.uniparthenope.reconosp:showBreadcrumbs}</code></td><td></td></tr>
           </table>
           @see #CarouselViewPager_animateSelection
           @see #CarouselViewPager_pageMargin
           @see #CarouselViewPager_pageWidth
           @see #CarouselViewPager_showBreadcrumbs
         */
        public static final int[] CarouselViewPager = {
            0x7f010003, 0x7f010004, 0x7f010005, 0x7f010006
        };
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#animateSelection}
          attribute's value can be found in the {@link #CarouselViewPager} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name it.uniparthenope.reconosp:animateSelection
        */
        public static final int CarouselViewPager_animateSelection = 1;
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#pageMargin}
          attribute's value can be found in the {@link #CarouselViewPager} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name it.uniparthenope.reconosp:pageMargin
        */
        public static final int CarouselViewPager_pageMargin = 2;
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#pageWidth}
          attribute's value can be found in the {@link #CarouselViewPager} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name it.uniparthenope.reconosp:pageWidth
        */
        public static final int CarouselViewPager_pageWidth = 3;
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#showBreadcrumbs}
          attribute's value can be found in the {@link #CarouselViewPager} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name it.uniparthenope.reconosp:showBreadcrumbs
        */
        public static final int CarouselViewPager_showBreadcrumbs = 0;
        /** Attributes that can be used with a HudItemView.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #HudItemView_label it.uniparthenope.reconosp:label}</code></td><td></td></tr>
           <tr><td><code>{@link #HudItemView_mark it.uniparthenope.reconosp:mark}</code></td><td></td></tr>
           <tr><td><code>{@link #HudItemView_value it.uniparthenope.reconosp:value}</code></td><td></td></tr>
           </table>
           @see #HudItemView_label
           @see #HudItemView_mark
           @see #HudItemView_value
         */
        public static final int[] HudItemView = {
            0x7f010007, 0x7f010008, 0x7f010009
        };
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#label}
          attribute's value can be found in the {@link #HudItemView} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name it.uniparthenope.reconosp:label
        */
        public static final int HudItemView_label = 1;
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#mark}
          attribute's value can be found in the {@link #HudItemView} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name it.uniparthenope.reconosp:mark
        */
        public static final int HudItemView_mark = 2;
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#value}
          attribute's value can be found in the {@link #HudItemView} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name it.uniparthenope.reconosp:value
        */
        public static final int HudItemView_value = 0;
        /** Attributes that can be used with a ReconTextView.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #ReconTextView_fontResource it.uniparthenope.reconosp:fontResource}</code></td><td></td></tr>
           </table>
           @see #ReconTextView_fontResource
         */
        public static final int[] ReconTextView = {
            0x7f01000a
        };
        /**
          <p>This symbol is the offset where the {@link it.uniparthenope.reconosp.R.attr#fontResource}
          attribute's value can be found in the {@link #ReconTextView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name it.uniparthenope.reconosp:fontResource
        */
        public static final int ReconTextView_fontResource = 0;
    };
}
